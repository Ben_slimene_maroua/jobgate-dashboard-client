import { Component, OnInit } from '@angular/core';
import { PlaceService } from '../place.service';

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.css']
})
export class PlaceComponent implements OnInit {
listplaces:any;
  constructor(private placeservice:PlaceService) { }

  ngOnInit(): void {
   this.getPlaces()
  }
getPlaces(){
  return this.placeservice.getAllPlaces().subscribe((res:any)=>{
this.listplaces=res['data'];
console.log(this.listplaces)
  })
}
}
