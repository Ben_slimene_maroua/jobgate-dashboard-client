import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {
  constructor(private http: HttpClient) { }

  getallentreprises(){
    return this.http.get(`${environment.baseUrl}/admin/getentreprise`);
  }
  getEntreprise(id:any){
return this.http.get(`${environment.baseUrl}/admin/getUserById/${id}`);
  }
}
