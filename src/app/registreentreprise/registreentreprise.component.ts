import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegistreService } from '../registre.service';
import { SpecialiteService } from '../specialite.service';

@Component({
  selector: 'app-registreentreprise',
  templateUrl: './registreentreprise.component.html',
  styleUrls: ['./registreentreprise.component.css']
})
export class RegistreentrepriseComponent implements OnInit {
  regForm: FormGroup;
  submitted=false;
  listspecialite:any
  fileToUpload:Array<File>=[]; 
  constructor(private regitreservice: RegistreService,private specialiteservice:SpecialiteService,private formbuilder: FormBuilder, private route:Router) { }

  ngOnInit(): void {
    this.regForm=this.formbuilder.group({
      nom:['',Validators.required],
      siteweb:['',Validators.required],
     role:['',Validators.required],
      email:['',Validators.required],
      password:['',Validators.required],
      telephone:['',Validators.required],
      adresse:['',Validators.required],
      description:['',Validators.required],
      specialites:['',Validators.required],
    });

    this.getallspecialite();
  }
  handleFileInput(files:any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload)}

  get f() { return this.regForm.controls; }
  onSubmit() {
    this.submitted = true;
 let formdata= new FormData();
      formdata.append("nom",this.regForm.value.nom);
      formdata.append("telephone",this.regForm.value.telephone);
      formdata.append("adresse",this.regForm.value.adresse);
      formdata.append("siteweb",this.regForm.value.siteweb);
      formdata.append("description",this.regForm.value.description);
      formdata.append("role",this.regForm.value.role);
      formdata.append("specialites",this.regForm.value.specialites);
      formdata.append("email",this.regForm.value.email);
      formdata.append("password",this.regForm.value.password);
      formdata.append("image",this.fileToUpload[0]);
    this.regitreservice.addUsers(formdata).subscribe((res:any)=>{
      console.log("response",res)
      Swal.fire("user ajouté avec succé")
  })
 this.route.navigateByUrl('/')
}

onReset() {
  this.submitted = false;
  this.regForm.reset();
}

getallspecialite() {
  this.specialiteservice.getSpecialite().subscribe((res: any) => {
    this.listspecialite = res['data'];
    console.log('list specialites', this.listspecialite);
  });
}
}

