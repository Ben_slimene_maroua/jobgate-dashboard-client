import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistreentrepriseComponent } from './registreentreprise.component';

describe('RegistreentrepriseComponent', () => {
  let component: RegistreentrepriseComponent;
  let fixture: ComponentFixture<RegistreentrepriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistreentrepriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistreentrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
