import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { EntrepriseService } from '../entreprise.service';
import { OffreService } from '../offre.service';
import { PlaceService } from '../place.service';
import { SpecialiteService } from '../specialite.service';

@Component({
  selector: 'app-addoffre',
  templateUrl: './addoffre.component.html',
  styleUrls: ['./addoffre.component.css']
})
export class AddoffreComponent implements OnInit {

  postjobForm: FormGroup;
  submitted = false;
 specialite: any ;
  listspecialite: any;
  listplaces : any;
  listeskils: any;
  listentreprises:any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  
  constructor(private formBuilder: FormBuilder, private serviceoffre:OffreService, private specialiteservice:SpecialiteService, private serviceplace :PlaceService, private entrepriseservice:EntrepriseService) { }
  ngOnInit() {
    this.postjobForm = this.formBuilder.group({
      titre: ['', Validators.required],
      type:['', Validators.required],
      date: ['', Validators.required],
      description: ['', Validators.required],
      specialite: ['', Validators.required],
      place: ['', Validators.required],
      entreprise: ['', Validators.required],
    });
  this.getallspecialite();
  this.getPlaces();
  this.getallentreprise();
}

addoffree() {
    this.submitted = true;
    this.postjobForm.patchValue({
      entreprise: this.userconnect._id,})

this.serviceoffre.addoffre(this.postjobForm.value).subscribe((res:any)=>{ 
  console.log("response",res)
  Swal.fire('Offre ajouté ')
})
}
onReset() {
    this.submitted = false;
    this.postjobForm.reset();
}
getallspecialite() {
  this.specialiteservice.getSpecialite().subscribe((res: any) => {
    this.listspecialite = res['data'];
    console.log('list categorys', this.listspecialite);
  });
}
getPlaces(){
  return this.serviceplace.getAllPlaces().subscribe((res:any)=>{
this.listplaces=res['data'];
console.log(this.listplaces)
  })
}

getallentreprise() {
  this.entrepriseservice.getallentreprises().subscribe((res: any) => {
    this.listentreprises = res['data'];
    console.log('list entreprise', this.listentreprises);
    // this.getoneentreprise(this.listentreprises[0]._id);
  });
}
}