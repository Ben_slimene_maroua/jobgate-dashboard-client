import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';
import { SpecialiteService } from '../specialite.service';

@Component({
  selector: 'app-offre',
  templateUrl: './offre.component.html',
  styleUrls: ['./offre.component.css']
})
export class OffreComponent implements OnInit {
  listoffre:any;
  updateoffreForm: FormGroup;
  closeResult = '';
  specialite: any;
  search_nom: any;
  p:number=1;

  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor(private offreservice: OffreService,  private formBuilder: FormBuilder,
    private modalService: NgbModal, private spservice:SpecialiteService) { }
  ngOnInit(): void {
    this.getspecialites();
    this.getalloffres();

    this.updateoffreForm = this.formBuilder.group({
      _id: ['', Validators.required],
      titre: ['', Validators.required],
      type: ['', Validators.required],
      date: ['', Validators.required],
      description: ['', Validators.required],
    });
   
  }
  getalloffres() {
   return this.offreservice.getallOffre().subscribe((res:any)=>{
    this.listoffre=res['data'].filter(
      (element: any) => element.verifier== true
    );
    console.log('liste offre :', this.listoffre);
  });
  
  }
  deleteoffre(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
  this.offreservice.deleteOffre(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getalloffres();
        });
        
      }
     
    });
  
  }
  updateoffre() {
  return  this.offreservice
      .updateoffre(this.updateoffreForm.value._id, this.updateoffreForm.value)
      .subscribe((res: any) => {
        Swal.fire('offre modifiée');
        this.getalloffres();
      });
  
  }
  open(content: any, offre: any) {
    this.updateoffreForm.patchValue({
      _id: offre._id,
      titre: offre.titre,
     type: offre.type,
     date: offre.date,
     description: offre.description,
    });
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  Onchangespecialite(event: any) {
    console.log('detected value ', event.target.value);
   return this.offreservice.getallOffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.specialite.nom == event.target.value
      );
      console.log('liste offre by specialites', this.listoffre);
    });
  }

  Onchangeplaces(event: any) {
    console.log('detected value ', event.target.value);
   return this.offreservice.getallOffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.place.adresse == event.target.value
      );
      console.log('liste offre by place', this.listoffre);
    });
  }

  Onchangetype(event: any) {
    console.log('detected value ', event.target.value);
   return this.offreservice.getallOffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.type == event.target.value
      );
      console.log('liste offre by type', this.listoffre);
    });
  }
  getspecialites() {
 return   this.spservice.getSpecialite().subscribe((res: any) => {
      this.specialite = res['data'];
      console.log('specialites', this.specialite);
    });
  }

  isentreprise(){
    return this.userconnect.role=="entreprise" ?true:false;
  }
}