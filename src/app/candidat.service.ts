import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CandidatService {
  constructor(private http: HttpClient) { }

  getallcandidats(){
    return this.http.get(`${environment.baseUrl}/admin/getentreprise`);
  }
  getCandidat(id:any){
return this.http.get(`${environment.baseUrl}/admin/getUserById/${id}`);
  }
 
  deleteUser(id:any){
   return this.http.delete(`${environment.baseUrl}/admin/deleteuser/${id}`)
  }
}