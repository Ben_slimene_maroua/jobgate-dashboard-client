import { NgModule } from '@angular/core';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { RegistreComponent } from './registre/registre.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { OffreComponent } from './offre/offre.component';
import { SpecialiteComponent } from './specialite/specialite.component';
import { RecherchePipe } from './pipes/recherche.pipe';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { DetailOffreComponent } from './detail-offre/detail-offre.component';
import { AddoffreComponent } from './addoffre/addoffre.component';
import { PlaceComponent } from './place/place.component';
import { DetailEntrepriseComponent } from './detail-entreprise/detail-entreprise.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CandidatComponent } from './candidat/candidat.component';
import { DetailcandidatComponent } from './detailcandidat/detailcandidat.component';
import { ContactComponent } from './contact/contact.component';
import { CandidatureComponent } from './candidature/candidature.component';
import { CvComponent } from './cv/cv.component';
import { DetailCandidatureComponent } from './detail-candidature/detail-candidature.component';
import { EspaceEntrepriseComponent } from './espace-entreprise/espace-entreprise.component';
import { MessageComponent } from './message/message.component';
import { ListmessageComponent } from './listmessage/listmessage.component';
import { DetailMessageComponent } from './detail-message/detail-message.component';
import { RegistreentrepriseComponent } from './registreentreprise/registreentreprise.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    HomeComponent,
    RegistreComponent,
    LoginComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    OffreComponent,
    SpecialiteComponent,
    RecherchePipe,
    EntrepriseComponent,
    DetailOffreComponent,
    AddoffreComponent,
    PlaceComponent,
    DetailEntrepriseComponent,
    CandidatComponent,
    DetailcandidatComponent,
    ContactComponent,
    CandidatureComponent,
    CvComponent,
    DetailCandidatureComponent,
    EspaceEntrepriseComponent,
    MessageComponent,
    ListmessageComponent,
    DetailMessageComponent,
    RegistreentrepriseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgxPaginationModule,
    PdfViewerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
