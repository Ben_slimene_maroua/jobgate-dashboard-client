import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-detail-message',
  templateUrl: './detail-message.component.html',
  styleUrls: ['./detail-message.component.css']
})
export class DetailMessageComponent implements OnInit {
  id=this.activeroute.snapshot.params['id'];
  message: any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  constructor(private msgservice:MessageService, private activeroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getMsg();
  }
getMsg(){
  return this.msgservice.getmessage(this.id).subscribe((res:any)=>{
this.message=res['data'],
console.log(this.message);
  })
}
}
