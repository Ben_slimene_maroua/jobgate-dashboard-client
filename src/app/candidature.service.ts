import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CandidatureService {

  constructor( private http: HttpClient) { }

  getallcandidatures(){
    return this.http.get(`${environment.baseUrl}/candidature/getallcandidature`);
  }
  getcandidature(id:any){
    return this.http.get(`${environment.baseUrl}/candidature/getcandidaturebyid/${id}`);
  
  }
  createCandidature(candidature:any){
    return this.http.post(`${environment.baseUrl}/candidature/createcandidature`,candidature);
  }

  deleteCandidature(id:any){
    return this.http.delete(`${environment.baseUrl}/candidature/deletecandidature/${id}`);
  }
  updateCandidature(id:any,candidature:any){
    return this.http.put(`${environment.baseUrl}/candidature/updatecandidature/${id}`,candidature);
  }
}
