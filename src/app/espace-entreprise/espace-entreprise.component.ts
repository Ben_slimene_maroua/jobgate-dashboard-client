import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';
import { SpecialiteService } from '../specialite.service';

@Component({
  selector: 'app-espace-entreprise',
  templateUrl: './espace-entreprise.component.html',
  styleUrls: ['./espace-entreprise.component.css']
})
export class EspaceEntrepriseComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  listoffre:any;
  search_nom: any;
  p:number=1;
  specialite:any;
   constructor(private offreservice: OffreService,private spservice:SpecialiteService) { }
 
   ngOnInit(): void {
     this.getOffre();
     this.getspecialites();
   }
  
 
   getOffre(){
     return this.offreservice.getallOffre().subscribe((res:any)=>{
       this.listoffre= res['data'].filter(
         (element: any) => element.entreprise._id == this.userconnect._id
       );
     })
   }
   deleteoffre(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
  this.offreservice.deleteOffre(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getOffre();
        });
        
      }
     
    });
  
  }
  Onchangespecialite(event: any) {
    console.log('detected value ', event.target.value);
   return this.offreservice.getallOffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.specialite.nom == event.target.value
      );
      console.log('liste offre by specialites', this.listoffre);
    });
  }

  Onchangeplaces(event: any) {
    console.log('detected value ', event.target.value);
   return this.offreservice.getallOffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.place.adresse == event.target.value
      );
      console.log('liste offre by place', this.listoffre);
    });
  }

  Onchangetype(event: any) {
    console.log('detected value ', event.target.value);
   return this.offreservice.getallOffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.type == event.target.value
      );
      console.log('liste offre by type', this.listoffre);
    });
  }
  getspecialites() {
 return   this.spservice.getSpecialite().subscribe((res: any) => {
      this.specialite = res['data'];
      console.log('specialites', this.specialite);
    });
  }
 }
 
