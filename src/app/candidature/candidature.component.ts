import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { CandidatureService } from '../candidature.service';

@Component({
  selector: 'app-candidature',
  templateUrl: './candidature.component.html',
  styleUrls: ['./candidature.component.css']
})
export class CandidatureComponent implements OnInit {
  addcondidatureForm: FormGroup;
  submitted= false;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  id=this.activeroute.snapshot.params['id'];
  fileToUpload:Array<File>=[]; 
  constructor(private candidatureservice: CandidatureService , private formbuilder: FormBuilder,private activeroute: ActivatedRoute ) { }

  ngOnInit(): void {

  this.addcondidatureForm = this.formbuilder.group({
    date: ['', Validators.required],
    cv:['', Validators.required],
    candidat: ['', Validators.required],
    id_offre: ['', Validators.required],
  });
}
  handleFileInput(files:any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload)
  }

  Addcandidature(){
  this.submitted = true;

  let formdata = new FormData();
  formdata.append('candidat', this.userconnect._id);
  formdata.append('id_offre', this.id);
formdata.append('cv', this.fileToUpload[0]);

  this.candidatureservice.createCandidature(formdata).subscribe((res: any) => {
   
    console.log('response', res);
    Swal.fire('Candidature ajouter'); 
  });

}


onReset() {
  this.submitted = false;
  this.addcondidatureForm.reset();
}
}

