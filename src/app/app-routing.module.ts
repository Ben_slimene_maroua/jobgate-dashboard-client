import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddoffreComponent } from './addoffre/addoffre.component';
import { CandidatComponent } from './candidat/candidat.component';
import { CandidatureComponent } from './candidature/candidature.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ContactComponent } from './contact/contact.component';
import { CvComponent } from './cv/cv.component';
import { DetailCandidatureComponent } from './detail-candidature/detail-candidature.component';
import { DetailEntrepriseComponent } from './detail-entreprise/detail-entreprise.component';
import { DetailMessageComponent } from './detail-message/detail-message.component';
import { DetailOffreComponent } from './detail-offre/detail-offre.component';
import { DetailcandidatComponent } from './detailcandidat/detailcandidat.component';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { EspaceEntrepriseComponent } from './espace-entreprise/espace-entreprise.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { ListmessageComponent } from './listmessage/listmessage.component';
import { LoginComponent } from './login/login.component';
import { MessageComponent } from './message/message.component';
import { OffreComponent } from './offre/offre.component';
import { RegistreComponent } from './registre/registre.component';
import { RegistreentrepriseComponent } from './registreentreprise/registreentreprise.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


const routes: Routes = [
  {path:'', component:LoginComponent},
  { path: 'registre', component: RegistreComponent },
  {path:'regEntreprise', component:RegistreentrepriseComponent},
  {path:'forgetPassword',component:ForgetPasswordComponent},
  {path:'resetpassword',component:ResetPasswordComponent},
 

  {
    path: 'home',canActivate:[AuthGuard],
    component: HomeComponent,
    children: [
      { path: '', component: LayoutComponent },
     
      {path:'changepwd',component:ChangePasswordComponent},
      {path:'resetpwd',component:ResetPasswordComponent},
      {path:'offre',component:OffreComponent},
      {path:'detailoffre/:id',component:DetailOffreComponent},
      {path:'addoffre', component:AddoffreComponent}, 
      {path:'entreprise',component: EntrepriseComponent},
      { path: 'detailentreprise/:id', component:DetailEntrepriseComponent },
      {path:'candidat', component: CandidatComponent},
      {path:'detailcandidat/:id',component:DetailcandidatComponent},
      {path:'contact', component:ContactComponent},
      {path:'candidature/:id', component:CandidatureComponent},
      {path:'cv/:id',component:CvComponent},
      {path:'detailcandidature/:id', component:DetailCandidatureComponent}, 
      {path:'espaceentreprise', component: EspaceEntrepriseComponent},
      {path:'message/:id', component:MessageComponent},
      {path:'listmsg/:id', component: ListmessageComponent}, 
      {path:'detailmessage/:id', component:DetailMessageComponent},
    

     
    ],
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
