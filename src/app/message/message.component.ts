import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
msgForm: FormGroup;
submitted = false;
listmsg: any;
id=this.activeroute.snapshot.params['id'];
userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  constructor(private msgservice : MessageService, private formbuilder: FormBuilder, private activeroute : ActivatedRoute) { }

  ngOnInit(): void {
    this.msgForm = this.formbuilder.group({
      titre: ['', Validators.required],
      description: ['', Validators.required],
      entreprise: ['', Validators.required],
      candidat:['', Validators.required],
    });
  }


  addMsg() {
    this.submitted = true;
    this.msgForm.patchValue({
      entreprise: this.id,
      candidat: this.userconnect._id,
    })
this.msgservice.addmessage(this.msgForm.value).subscribe((res:any)=>{ 
  console.log("response",res)
  Swal.fire('message envoyé ')
})
}
onReset() {
    this.submitted = false;
    this.msgForm.reset();
}

}
