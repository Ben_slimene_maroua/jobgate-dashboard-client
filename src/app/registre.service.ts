import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistreService {
  constructor(private http: HttpClient) { }
  addUsers(user:any){
 return this.http.post(`${environment.baseUrl}/registre/user`, user);
  } 
 }