import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CandidatureService } from '../candidature.service';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {
  id=this.activeroute.snapshot.params['id'];
  candidature:any
  pdfSrc:any
  constructor(private candidatureservice: CandidatureService , private activeroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getcandidature();
  }
getcandidature(){
  return this.candidatureservice.getcandidature(this.id).subscribe((res:any)=>{
this.candidature=res['data'];
console.log(this.candidature);
this.pdfSrc = 'http://localhost:3000/getfile/' + this.candidature.cv;
  })
}
}
