import { Component, OnInit } from '@angular/core';

import { OffreService } from '../offre.service';
import { SpecialiteService } from '../specialite.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  listoffre:any;
  specialite:any;
  search_nom: any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  constructor(private offreservice: OffreService,  private spservice:SpecialiteService) { }

  ngOnInit(): void {
    this.getalloffres();
    this.getspecialites();
  }
  getalloffres() {
    return this.offreservice.getallOffre().subscribe((res:any)=>{
     this.listoffre=res['data']
     console.log('liste offre :', this.listoffre);
   });
   
   }

   getspecialites() {
    return   this.spservice.getSpecialite().subscribe((res: any) => {
         this.specialite = res['data'];
         console.log('specialites', this.specialite);
       });
     }

   Onchangespecialite(event: any) {
    console.log('detected value ', event.target.value);
   return this.offreservice.getallOffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.specialite.nom == event.target.value
      );
      console.log('liste offre by specialites', this.listoffre);
    });
  }

  Onchangeplaces(event: any) {
    console.log('detected value ', event.target.value);
   return this.offreservice.getallOffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.place.adresse == event.target.value
      );
      console.log('liste offre by place', this.listoffre);
    });
  }
  iscandidat(){
    return this.userconnect.role=="candidat" ?true:false;
  }
}
