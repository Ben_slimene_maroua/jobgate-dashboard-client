import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { CandidatureService } from '../candidature.service';

@Component({
  selector: 'app-detail-candidature',
  templateUrl: './detail-candidature.component.html',
  styleUrls: ['./detail-candidature.component.css']
})
export class DetailCandidatureComponent implements OnInit {
  listcandidat: any
  id=this.activeroute.snapshot.params['id'];
  constructor(private candidaturesevice: CandidatureService, private activeroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getcandidature();
  }
  getcandidature() {
    this.candidaturesevice.getallcandidatures().subscribe((res: any) => {
      console.log("res",res)
      this.listcandidat = res ["data"].filter(
        (element: any) => element.id_offre._id == this.id
      );
      console.log('list candidat', this.listcandidat);
    });
  }
  
  deletecandidature(id: any) {
  
  this.candidaturesevice.deleteCandidature(id).subscribe((res: any) => {
         
          this.getcandidature();
        });
  }
     
 
  
  
}
