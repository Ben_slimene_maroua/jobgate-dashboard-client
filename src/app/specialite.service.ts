import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpecialiteService {

  constructor(private http: HttpClient) { }

  getSpecialite(){
    return this.http.get(`${environment.baseUrl}/specialite/getspecialite`);
  }; 
}

