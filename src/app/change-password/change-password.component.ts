import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ForgetPasswordService } from '../forget-password.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  ChangeForm: FormGroup;
  constructor(private forgetservice:ForgetPasswordService, private formbuilder: FormBuilder) { }

  ngOnInit(): void {
    this.ChangeForm=this.formbuilder.group({
      oldpassword:['',Validators.required],
      newpassword:['',Validators.required]
    })
  }
ChangePassword(){
  return this.forgetservice.changePassword(this.ChangeForm.value).subscribe((res:any)=>{
    Swal.fire('Mot de passe modifié', 'success');
  })
}
}
