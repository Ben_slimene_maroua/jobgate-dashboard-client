import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OffreService {
  constructor(private http: HttpClient) { }
  token=localStorage.getItem('token')!;
  headersoption= new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallOffre(){
    return this.http.get(`${environment.baseUrl}/offre/getoffre`);
  }
  getoffre(id:any){
return this.http.get(`${environment.baseUrl}/offre/getoffreById/${id}`);
  }

  deleteOffre(id:any){
    return this.http.delete(`${environment.baseUrl}/offre/deleteoffre/${id}`,{headers: this.headersoption})
  }
  updateoffre(id:any,offre:any){
    return this.http.put(`${environment.baseUrl}/offre/upoffre/${id}`,offre,{headers: this.headersoption})
  }
  addoffre(offre:any){
    return this.http.post(`${environment.baseUrl}/offre/createoffre`,offre,{headers: this.headersoption})
  }
}