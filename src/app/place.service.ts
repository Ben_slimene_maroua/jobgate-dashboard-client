import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  constructor(private http:HttpClient) { }
  getAllPlaces(){
    return this.http.get(`${environment.baseUrl}/places/getplaces`);
  }
}
